#plot
import os
import csv
from matplotlib import pyplot as plt

def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter=',')
        header = next(row)  # read first line
        data = []
        for r in row:
            r[2:] = map(float, r[2:])
            data.append(r)
    return data


def plot(course1_costs, course2_costs, course3_costs):
    plt.figure(figsize=(18, 8))

    plt.subplot(311)
    plt.plot(course1_costs, color='blue')

    plt.subplot(312)
    plt.plot(course2_costs, color='green')

    plt.subplot(313)
    plt.plot(course3_costs, color='red')
    plt.show()


if __name__ == '__main__':
    data = read_data(os.path.join('..\data', 'part1.csv'))
    course1 = [item[2] for item in data]
    course2 = [item[3] for item in data]
    course3 = [item[4] for item in data]
    plot(course1, course2, course3)